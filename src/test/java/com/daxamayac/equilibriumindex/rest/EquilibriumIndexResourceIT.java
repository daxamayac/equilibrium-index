package com.daxamayac.equilibriumindex.rest;

import com.daxamayac.equilibriumindex.data.EquilibriumIndexEntity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EquilibriumIndexResourceIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testEquilibriumIndexResource() {
        List<Integer> elements = Arrays.asList(-7, 1, 5, 2, -4, 3, 0);
        EquilibriumIndexEntity response = this.restTemplate
                .postForEntity(EquilibriumIndexResource.EQUILIBRIUM_INDICES, elements, EquilibriumIndexEntity.class)
                .getBody();

        assert response != null;
        assertEquals(3, response.getEquilibriumIndex());
    }

    @Test
    void testEquilibriumIndexResourceArrayEmpty() {
        List<Integer> elements = Collections.emptyList();
        EquilibriumIndexEntity response = this.restTemplate
                .postForEntity(EquilibriumIndexResource.EQUILIBRIUM_INDICES, elements, EquilibriumIndexEntity.class)
                .getBody();
        assert response != null;
        assertEquals(-1, response.getEquilibriumIndex());
    }

    @Test
    @AfterAll
    void testReadEquilibriumIndicesHistory() {
        List<Integer> elements = Arrays.asList(-7, 1, 5, 2, -4, 3, 0);

        EquilibriumIndexEntity[] response = this.restTemplate
                .getForEntity(EquilibriumIndexResource.EQUILIBRIUM_INDICES, EquilibriumIndexEntity[].class)
                .getBody();

        assert response != null;
        assertTrue(Arrays.asList(response).toString().contains(elements.toString()));
    }
}

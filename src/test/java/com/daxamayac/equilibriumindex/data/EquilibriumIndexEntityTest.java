package com.daxamayac.equilibriumindex.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EquilibriumIndexEntityTest {

    private EquilibriumIndexEntity equilibriumIndexEntity;

    @BeforeEach
    void before() {
        List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5);
        this.equilibriumIndexEntity = new EquilibriumIndexEntity(elements);
    }

    @Test
    void testEquilibriumIndexNoExist() {
        assertEquals(-1, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }

    @Test
    void testEquilibriumIndexNoExistFromNull() {
        this.equilibriumIndexEntity.setElements(null);
        assertEquals(-1, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }

    @Test
    void testEquilibriumIndexNoExistFromEmptyElements() {
        List<Integer> elements = Collections.emptyList();
        this.equilibriumIndexEntity.setElements(elements);
        assertEquals(-1, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }

    @Test
    void testEquilibriumIndexIsP0() {
        List<Integer> elements = Arrays.asList(0, 2, -5, 8, 3, -6, -2);
        this.equilibriumIndexEntity.setElements(elements);
        assertEquals(0, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }

    @Test
    void testEquilibriumIndexIsPN_1() {
        List<Integer> elements = Arrays.asList(-7, 3, 5, -4, 3, 0);
        this.equilibriumIndexEntity.setElements(elements);
        assertEquals(5, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }

    @Test
    void testFirstEquilibriumIndexExist() {
        List<Integer> elements = Arrays.asList(-7, 1, 5, 2, -4, 3, 0);
        this.equilibriumIndexEntity.setElements(elements);
        assertEquals(3, this.equilibriumIndexEntity.calculateEquilibriumIndex());
    }
}

package com.daxamayac.equilibriumindex.rest;

import com.daxamayac.equilibriumindex.data.EquilibriumIndexEntity;
import com.daxamayac.equilibriumindex.services.EquilibriumIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @author daxamayac
 * @since 1.0.0
 */
@RestController
@RequestMapping(EquilibriumIndexResource.EQUILIBRIUM_INDICES)
public class EquilibriumIndexResource {
    /**
     * path resource
     */
    public static final String EQUILIBRIUM_INDICES = "/equilibrium-indices";

    private EquilibriumIndexService equilibriumIndexService;

    @Autowired
    public EquilibriumIndexResource(EquilibriumIndexService equilibriumIndexService) {
        this.equilibriumIndexService = equilibriumIndexService;
    }

    /**
     *
     * @param elements integer list
     * @return json
     */
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public EquilibriumIndexEntity createEquilibriumIndex(@RequestBody List<Integer> elements) {
        return equilibriumIndexService.calculeEquilibriumIndex(elements);
    }

    /**
     *
     * @return History of equilibrium indices calculated
     */
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<EquilibriumIndexEntity> readAllEquilibriumIndices() {
        return equilibriumIndexService.readAllEquilibriumIndices();
    }
}

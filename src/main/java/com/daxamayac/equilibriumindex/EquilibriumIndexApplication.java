package com.daxamayac.equilibriumindex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EquilibriumIndexApplication {

    public static void main(String[] args) {
        SpringApplication.run(EquilibriumIndexApplication.class, args);
    }

}

package com.daxamayac.equilibriumindex.services;

import com.daxamayac.equilibriumindex.data.EquilibriumIndexEntity;
import com.daxamayac.equilibriumindex.data.EquilibriumIndexRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author daxamayac
 * @since 1.0.0
 */
@Service
public class EquilibriumIndexService {

    private final EquilibriumIndexRepository equilibriumIndexRepository;

    @Autowired
    public EquilibriumIndexService(EquilibriumIndexRepository equilibriumIndexRepository) {
        this.equilibriumIndexRepository = equilibriumIndexRepository;
    }

    /**
     * Calculate equilibrium index and save to database
     *
     * @param elements to calculate equilibrium index
     * @return stored equilibrium index
     */
    public EquilibriumIndexEntity calculeEquilibriumIndex(List<Integer> elements) {
        var equilibriumIndexEntity = new EquilibriumIndexEntity(elements);
        equilibriumIndexEntity.calculateEquilibriumIndex();
        return equilibriumIndexRepository.save(equilibriumIndexEntity);
    }

    /**
     * @return History of equilibrium indices calculated
     */
    public List<EquilibriumIndexEntity> readAllEquilibriumIndices() {
        return equilibriumIndexRepository.findAll();
    }
}

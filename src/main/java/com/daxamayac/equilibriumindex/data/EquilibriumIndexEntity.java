package com.daxamayac.equilibriumindex.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author daxamayac
 * @since 1.0.0
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class EquilibriumIndexEntity {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;
    private ZonedDateTime requestDate;
    /**
     * Elements to calculate the equilibrium index
     */
    @ElementCollection
    private List<Integer> elements;
    private Integer equilibriumIndex = -1;

    public EquilibriumIndexEntity(List<Integer> elements) {
        this.requestDate = ZonedDateTime.now();
        this.elements = elements;
    }

    /**
     *
     * @return Equilibrium Index or -1 if no found
     */
    public int calculateEquilibriumIndex() {

        if (this.elements == null || this.elements.isEmpty())
            return -1;

        var sumLeft = 0;
        int sumRight = this.elements.stream().reduce(0, Integer::sum);

        for (var index = 0; index < this.elements.size(); index++) {
            sumRight -= this.elements.get(index);

            if (sumLeft == sumRight) {
                this.equilibriumIndex = index;
                return index;
            }
            sumLeft += this.elements.get(index);
        }
        return -1;
    }

}

package com.daxamayac.equilibriumindex.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquilibriumIndexRepository extends JpaRepository<EquilibriumIndexEntity, Long> {
}

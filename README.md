# Equilibrium index - El índice de equilibrio  

`Java-11` `Gradle` `Spring Boot 2.5.0` `Spring Data` `lombok` `H2` `JUnit` `Sonarcloud` `Heroku`

>Disponemos de un array A compuesto por N enteros.  
El índice de equilibrio de este Array es cualquier entero P tal que 0 ≤ P < N y la suma de los
elementos de los índices inferiores es igual a la suma de los elementos de los índices superiores. Por ej:  
``A[0] + A[1] + … + A[P-1] = A[P+1] + … + A[N-2] + A[N-1]``  
La suma de 0 elementos se asume que es igual a 0. Esto puede ocurrir si P = 0 o si P = N-1.  
Por ej, considerando el siguiente array A que está compuesto por 7 elementos:  
``A[0] = -7 A[1] = 1 A[2] = 5 A[3] = 2 A[4] = -4 A[5] = 3 A[6] = 0``  
P = 3 es un índice de equilibrio de este array ya que  
``A[0] + A[1] + A[2] = A[4] + A[5] + A[6]``  
P = 6 también es un índice de equilibrio de este array ya que:  
``A[0] + A[1] + A[2] + A[3] + A[4] + A[5] = 0``  
La función debe devolver -1 si no existe un índice de equilibrio y el índice en caso de encontrarlo.    

***
<a href="https://gitlab.com/daxamayac/equilibrium-index/-/commits/develop"><img alt="pipeline status" src="https://gitlab.com/daxamayac/equilibrium-index/badges/develop/pipeline.svg" /></a>
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac_equilibrium-index&metric=alert_status)](https://sonarcloud.io/dashboard?id=daxamayac_equilibrium-index)
*** 
## :computer: Demo:  
### https://equilibrium-index.herokuapp.com/rest
***Esperar a que Heroku inicie el servicio***  
 
## :gear: Instalación del proyecto con **IntelliJ**

1. Importar los proyectos `equilibrium-index` mediante **IntelliJ**
    1. **File** >**New** > **Project from Version Control**, y pegar la URL del
       proyecto **https://gitlab.com/daxamayac/equilibrium-index.git**
    2. Click en *Clonar*
    3. Marcar como **Seguro** … **Finish**.  
    4. `Importante: Instalar lombok como plugin en IntelliJ`
2. Ejecución
    * Ejecución de test: Se realiza con H2 embebido
    * Ejecución en local:
        1. Spring. Ejecutar mediante línea de comando: `> gradlew bootRun` o Run con el botón del IDE
        2. El proyecto se ejecuta con H2 embebido
    * Ingresar a http://localhost:8080/rest para probar con el cliente de Swagger  
   
## :bulb: Que se podría mejorar?
* Investigar una optimización para el índice de equilibrio
* Hacer algo interesante con Spring Security y un usuario en memoria
* Simular una consulta lenta a la base de datos y hacer uso de Spring Cache

## :bulb: Evolución!
* Añadir más algoritmos del mismo tipo, reciban un array y retornen un entero
* Se puede hacer uso del Factory Pattern para obtener la instancia un algoritmo dependiendo del tipo (ENUM)
* Se puede almacenar tipo_de_algoritmo, fecha, elementos y resultado
